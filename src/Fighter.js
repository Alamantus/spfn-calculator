import { Dice } from './Dice';

export class Fighter {
  static Appearance1 = [
    'has a birthmark',
    'has spiky hair',
    'is very short',
    'is very tall',
    'is an alien',
    'has a tail',
  ];
  static Appearance2 = [
    'Gone',
    'Black',
    'Yellow',
    'Lavender',
    'Turquoise',
    'Red',
  ];
  static Appearance3 = [
    'Orange',
    'Pink',
    'White',
    'Teal',
    'Brown',
    'Gray',
  ];
  
  constructor({
    name = 'No Name',
    dodge = null,
    power = null,
    energy = null,
    heart = null,
    hp = null,
    appearance = null,
    appearance1 = null,
    appearance2 = null,
    appearance3 = null,
  } = {}) {
    this.name = name;

    const d = new Dice();
    this.dice = d;
    const stat = () => Math.floor(d.rollSingle() / 2);
    this.dodge = dodge ?? stat();
    this.power = power ?? stat();
    this.energy = energy ?? stat();
    this.heart = heart ?? d.rollSingle();
    if (!hp) {
      this.hp = 0;
      let hpDie;
      do {
        hpDie = d.rollSingle();
        this.hp += hpDie * this.heart * 100;
      } while (hpDie === 1);
    } else {
      this.hp = hp;
    }
    this.appearance = appearance ?? [
      appearance1 ?? d.rollSingle(),
      appearance2 ?? d.rollSingle(),
      appearance3 ?? d.rollSingle(),
    ];
    this.cooldown = 0;
    this.history = [];
  }

  describe() {
    if (Array.isArray(this.appearance)) {
      return `${this.name} ${Fighter.Appearance1[this.appearance[0]]}, their hair is ${Fighter.Appearance2[this.appearance[1]]}, and they wear a lot of ${Fighter.Appearance3[this.appearance[2]]}.`
    }
    return this.appearance;
  }

  initTurn() {
    if (this.cooldown > 0) this.cooldown--;
  }
  log(action, result, dice = null) {
    this.history.push({
      action,
      result,
      dice: dice ?? [...this.dice.lastRoll],
    });
  }

  physicalAttack() {
    const result = this.dice.rollSpread(this.power);
    this.log('physical', result);
    return result;
  }
  energyAttack() {
    if (this.cooldown > 0) {
      alert(`${this.name} must cool down for ${this.cooldown} more turn${this.cooldown === 1 ? '' : 's'}`);
      return null;
    }
    const { dice } = this;
    const cooldown = Math.ceil((dice.rollSingle() - this.energy) / 2);
    this.cooldown = cooldown < 0 ? 0 : cooldown;
    const result = dice.rollTotal(this.energy);
    this.log('energy', result);
    return result;
  }
  defend(attack) {
    const dodge = () => this.dice.rollTotal(this.dodge);
    let result, dice;
    if (Array.isArray(attack)) {
      dice = [];
      result = attack.map(() => {
        const d = dodge();
        dice.push([...this.dice.lastRoll]);
        return d;
      });
    } else {
      result = dodge();
      dice = [...this.dice.lastRoll];
    }
    this.log('dodge', result, dice);
    return result;
  }
  addHeart() {
    if (this.heart < 1) {
      alert(`${this.name} is out of Heart points!`);
      return null;
    }
    const { dice } = this;
    const result = dice.roll();
    this.log('heart', result);
    if (result[0] === this.heart) {
      this.heart++;
    } else {
      this.heart--;
    }
    return result;
  }
}