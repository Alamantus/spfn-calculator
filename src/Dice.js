export class Dice {
  constructor() {
    this.lastRoll = null;
    this.lastResult = null;
  }

  rollBase() {
    return Math.floor(Math.random() * 6);
  }
  rollSingle() {
    return this.rollBase() + 1;
  }

  roll() {
    const result = [];
    do {
      result.push(this.rollSingle())
    } while ([3, 6].includes(result[result.length - 1]));
    this.lastRoll = result;
    return result;
  }
  rollSpread(mod = 0) {
    const result = this.roll().map(roll => roll + mod);
    this.lastResult = result;
    return result;
  }
  rollTotal(mod = 0) {
    const result = this.roll().reduce((total, roll) => total + roll, 0) + mod;
    this.lastResult = result;
    return result;
  }
}