import { Fighter } from "./Fighter";

export class Game {
  static LocalStorageKey = 'spfndata';
  
  constructor() {
    let saved = window.localStorage.getItem(Game.LocalStorageKey);
    this.lastSaveHash = saved ? this.hashString(saved) : null;
    saved = saved ? JSON.parse(saved) : null;
    this.fighters = saved?.fighters?.map(data => new Fighter(data)) ?? [];
    this.foes = saved?.foes?.map(data => new Fighter(data)) ?? [];
    this.history = [];
    this.saveCheck = setInterval(() => {
      const json = this.toJson();
      const hash = this.hashString(json);
      if (this.lastSaveHash !== hash) {
        window.localStorage.setItem(Game.LocalStorageKey, json);
        this.lastSaveHash = hash;
      }
    }, 5000);
  }

  toJson() {
    return JSON.stringify({
      fighters: this.fighters,
      foes: this.foes,
    });
  }

  hashString(str) {
    let hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
      chr = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  createFighter(fighterData) {
    if (this.fighters.find(fighter => fighter.name === fighterData.name)) {
      alert('A fighter with the name "' + fighterData.name + '" already exists.');
      return null;
    }
    this.fighters.push(new Fighter(fighterData));
    return this.fighters[this.fighters.length - 1];
  }

  editFighter(fighterIndex, fighterData) {
    this.fighters[fighterIndex] = new Fighter(fighterData);
    return this.fighters[fighterIndex];
  }

  deleteFighter(fighterIndex) {
    return this.fighters.splice(fighterIndex, 1);
  }

  calcDamage(attack, defense) {
    let damage;
    const diff = (a, d) => {
      const r = a - d;
      return r < 0 ? 0 : r;
    }
    if (Array.isArray(defense)) {
      damage = attack.reduce((total, att, index) => total + diff(att, defense[index]), 0);
    } else {
      damage = diff(attack, defense);
    }
    return damage * 100;
  }
}