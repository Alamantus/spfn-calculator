import html from 'nanohtml';

export class Renderer {
  static renderFighters(fighters, fightersListElement) {
    fightersListElement.innerHTML = '';
    fighters.forEach(fighter => {
      fightersListElement.appendChild(html`<article class="box nogrow">
      <details>
        <summary style="font-weight:bold;cursor:pointer;">${fighter.name}</summary>
        ${fighter.describe()}
      </details>
      <ul>
        <li><strong>Hit Points:</strong> ${fighter.hp}</li>
        <li><strong>Dodge:</strong> ${fighter.dodge}</li>
        <li><strong>Power:</strong> ${fighter.power}</li>
        <li><strong>Energy:</strong> ${fighter.energy}
          ${fighter.cooldown > 0 ? html`<ul><li>Cooldown: ${fighter.cooldown} turn${fighter.cooldown === 1 ? '' : 's'}</li></ul>` : ''}
        </li>
        <li><strong>Heart:</strong> ${fighter.heart}</li>
      </ul>
      <button onclick=${() => window.page.editFighter(fighter)}>Edit</button>
      <button onclick=${() => window.page.openRollDialog(fighter)}>Roll</button>
      </article>`);
    });
  }

  static renderRollsDialog(fighter, element) {
    element.innerHTML = '';
    const rollsElement = html`<div style="max-height:70vh;overflow-y:auto;">
      ${fighter.history.map((roll, index) => html`<div style="border:1px solid black;border-radius:3px;padding:5px;margin-bottom:10px;${index === fighter.history.length - 1 ? 'background:#c0ffee;' : ''}">
        <strong>
          ${roll.action}
          ${roll.action === 'heart' ? `(${fighter.heart} remaining)` : ''}
        </strong><br>
        <p>Rolled: ${roll.dice.map(die => ['⚀', '⚁', '⚂', '⚃', '⚄', '⚅'][die - 1]).join(' ')}</p>
        <p>Result: ${Array.isArray(roll.result) ? roll.result.join(', ') : roll.result}</p>
      </div>`)}
    </div>`;
    element.appendChild(rollsElement);
    element.appendChild(html`<div>
    <button type="button" onclick=${() => {
      fighter.physicalAttack();
      Renderer.renderRollsDialog(fighter, element);
    }}>Physical Attack</button>
    <button type="button" onclick=${() => {
      fighter.energyAttack();
      Renderer.renderRollsDialog(fighter, element);
    }}>Energy Attack</button>
    <button type="button" disabled=${fighter.heart < 1} onclick=${() => {
      fighter.addHeart();
      Renderer.renderRollsDialog(fighter, element);
    }}>Spend Heart</button>
    </div>`);
    rollsElement.scrollTop = rollsElement.scrollHeight;
  }
}
