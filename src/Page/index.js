import { Game } from '../Game';
import { Fighter } from "../Fighter";
import { Renderer } from './Renderer';

export class Page {
  constructor() {
    this.elements = {};
    this.game = new Game();
    Array.from(document.querySelectorAll('[id]')).forEach(el => {
      console.log(el.id);
      this.elements[el.id] = el;
    });
  }

  renderFighters() {
    return Renderer.renderFighters(this.game.fighters, document.getElementById('fighterList'));
  }

  init() {
    this.initFighterForm();
    this.renderFighters();
  }

  initFighterForm() {
    const { addFighterBtn, fighterDialog } = this.elements;
    addFighterBtn.addEventListener('click', () => {
      fighterDialog.showModal();
    });
    const fighterForm = fighterDialog.querySelector('form');
    fighterForm.addEventListener('submit', e => {
      const form = e.target;
      const stats = {
        name: form.fighterName.value.trim(),
      };
      const fighterIndex = parseInt(form.fighterIndex.value);
      switch (e.submitter.value) {
        case 'cancel': return;
        case 'delete': {
          if (isNaN(fighterIndex)) return alert('fighterIndex not specified.');
          if (!confirm('Are you sure you want to delete ' + stats.name + '?')) return;
          return this.game.deleteFighter(fighterIndex);
        }
        case 'generate': break;
        case 'save': {
          stats.dodge = form.fighterDodge.value.length > 0 ? parseInt(form.fighterDodge.value) : null;
          stats.power = form.fighterPower.value.length > 0 ? parseInt(form.fighterPower.value) : null;
          stats.energy = form.fighterEnergy.value.length > 0 ? parseInt(form.fighterEnergy.value) : null;
          stats.heart = form.fighterHeart.value.length > 0 ? parseInt(form.fighterHeart.value) : null;
          stats.hp = form.fighterHp.value.length > 0 ? parseInt(form.fighterHp.value) : null;
          break;
        }
      }
      if (stats.name.length < 1) {
        alert ('Fighter must have a name.');
        return e.preventDefault();
      }
      if (isNaN(fighterIndex)) {
        this.game.createFighter(stats);
      } else {
        this.game.editFighter(fighterIndex, stats);
      }
    });
    fighterDialog.addEventListener('close', () => {
      Array.from(fighterForm.querySelectorAll('input')).forEach(el => {
        el.value = '';
      });
      fighterForm.querySelector('#generateFighter').style.display = '';
      fighterForm.querySelector('#deleteFighter').style.display = 'none';
      this.renderFighters();
    });
  }

  editFighter(fighter) {
    const fighterIndex = this.game.fighters.findIndex(f => f.name === fighter.name);
    if (fighterIndex < 0) {
      alert('Existing fighter with name "' + fighter.name + '" not found.');
      return null;
    }
    const { fighterDialog } = this.elements;
    const fighterForm = fighterDialog.querySelector('form');
    fighterForm.querySelector('#generateFighter').style.display = 'none';
    fighterForm.querySelector('#deleteFighter').style.display = '';
    fighterForm.fighterIndex.value = fighterIndex;
    fighterForm.fighterName.value = fighter.name;
    fighterForm.fighterDodge.value = fighter.dodge;
    fighterForm.fighterPower.value = fighter.power;
    fighterForm.fighterEnergy.value = fighter.energy;
    fighterForm.fighterHeart.value = fighter.heart;
    fighterForm.fighterHp.value = fighter.hp;
    fighterDialog.showModal();
  }
  
  openRollDialog(fighter) {
    const { rollDialog } = this.elements;
    rollDialog.querySelector('h3').innerText = `${fighter.name}'s Rolls`;
    Renderer.renderRollsDialog(fighter, rollDialog.querySelector('section'));
    rollDialog.showModal();
  }
}
